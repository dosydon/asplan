def sigalrm_handler(signum, frame):
    print('Signal handler called with signal', signum)
    raise TimeoutException("timeout")

class TimeoutException(Exception):
    pass
