import time
import subprocess
import psutil
import utils
import argparse
import os.path, sys
from log import *
from math import ceil

FILE_DIR = os.path.dirname(os.path.realpath(__file__))
PAR_DIR = os.path.realpath(os.path.join(FILE_DIR, os.pardir))

FD = os.path.join(PAR_DIR, 'fd-es/fast-downward.py')
FDAXIOM = os.path.join(PAR_DIR, 'fd-axiom/src/fast-downward.py')
CLINGO = os.path.join(PAR_DIR, 'clingo-4.5.4-source/build/release/clingo')
EXT_HOME = os.path.join(PAR_DIR, 'tau_axiom_extractor')
SAS2CLINGO = os.path.join(FILE_DIR ,'src/sas2clingo/sas2clingo')

class Command:
    def __init__(self,cmd,tag,**kwargs):
        self.cmd = cmd
        self.tag = tag
        self.kwargs = kwargs
    def proc(self,timeout=None,preexec_fn=None):
        return subprocess.Popen(self.cmd.split(' '),preexec_fn=preexec_fn,**self.kwargs)
    def __repr__(self):
        return (self.cmd)

class TranslateCmd(Command):
    def __init__(self,args):
#       if hasattr(args, 'translate_args'):
#           cmd = "{} --translate {} {} --translate-options {}".format(FD,args.dom_file,args.ins_file,args.translate_args)
#       else:
        cmd = "{} --translate {} {}".format(FDAXIOM,args.dom_file,args.ins_file)
        tag = Tag.TRANSLATE
        super().__init__(cmd,tag)

class OrigNameCmd(Command):
    def __init__(self,args):
        cmd = "{} sas_plan".format(ORIG_NAME)
        tag = Tag.TRANSLATE
        super().__init__(cmd,tag)

class EncodeCmd(Command):
    def __init__(self,args):
        cmd = "{}/encode.py output.sas --candidate_gen {}".format(EXT_HOME,args.candidate_gen)
        tag = Tag.ENCODE
        super().__init__(cmd,tag)

class DecodeCmd(Command):
    def __init__(self,args):
        cmd ="{}/decode.py output.sas sas_plan".format(EXT_HOME) 
        tag = Tag.DECODE
        super().__init__(cmd,tag)

class PreprocessCmd(Command):
    def __init__(self,args):
        cmd = "{}/preprocess.py output.sas".format(EXT_HOME)
        tag = Tag.PREPROCESS
        super().__init__(cmd,tag)

class Sas2ClingoCmd(Command):
    def __init__(self,args,**kwargs):
        cmd = "{} -i output.sas  -o {} -m {} -l {} -p {:d} -d {:d}".format(SAS2CLINGO,args.output,args.opt_level,args.para_level,args.pg,args.depth)
        tag = Tag.HP
        super().__init__(cmd,tag,**kwargs)

class ClingoCmd(Command):
    def __init__(self,args):
        cmd = "{} output.lp -n 1 --outf=2 --stats=2".format(CLINGO)
        tag = Tag.CLINGO
        super().__init__(cmd,tag)
    def proc(self,timeout=None,preexec_fn=None):
        if timeout:
            self.cmd += ' --time-limit={:d}'.format(int(ceil(timeout)))
        return subprocess.Popen(self.cmd.split(' '),stdout=subprocess.PIPE,preexec_fn=preexec_fn)

class MinionCmd(Command):
    def __init__(self,args,**kwargs):
        if len(args.var_selection) > 0:
            cmd = "{} {} -varorder {}".format(MINION,args.output,args.var_selection)
        else:
            cmd = "{} {}".format(MINION,args.output)
        tag = Tag.MINION
        super().__init__(cmd,tag,**kwargs)
    def proc(self,timeout=None,preexec_fn=None):
        return subprocess.Popen(self.cmd.split(' '),stdout=subprocess.PIPE,preexec_fn=preexec_fn)
