#include "pgraph.h"
#include "model.h"

using namespace std;

Depth
simple_extract_pg(const Problem *prob, SimplePG &simple_pg) {
	Depth lb = 0;

	auto vars = prob->vars;
	auto init = prob->init;

	unsigned int var_i = 0;
	for (auto var : vars) {
		for (Val val = 0; val < var.range; ++val) {
			Prop p(var_i, val);
			simple_pg[p] = init[var_i] == val ? 0 : PG_UNACTIVATED;
		}
		++var_i;
	}

	for (Depth t = 1;; ++t) {
		bool level_off = true;

		for (auto ope : prob->operators) {
			vector<Prop> posts, pres;

			for (auto ef : ope.effects) {
				Prop post(ef.var, ef.postval);
				Prop pre(ef.var, ef.preval);

				if (ef.preval != NOT_REQUIRED &&
				    simple_pg[pre] == PG_UNACTIVATED)
					goto next_operation;
				else
					posts.push_back(post);
			}

			/* CHECK: prevail conditions are used in what domain? */
			for (auto p : ope.prevailConditions)
				if (simple_pg[p] == PG_UNACTIVATED)
					goto next_operation;

			for (auto p : posts)
				if (simple_pg[p] == PG_UNACTIVATED) {
					simple_pg[p] = t;
					level_off = false;
				}

		next_operation:
			;
		}

		/* maybe possible to skip props which have already activated
		 * but guess it is not crucial */
		for (auto p : prob->goal)
			if (simple_pg[p] == PG_UNACTIVATED) {
				if (level_off)
					return -1; /* UNSAT */
				else
					goto next_depth;
			} else
				lb = t;

		if (level_off)
			return lb;

	next_depth:
		;
	}
}
