#include <iostream>
#include "config.h"

using namespace std;

static void
usage() {
	cout << "usage: hp [-optchar optval]...\n" << endl
	     << "-h: help" << endl
	     << "-d <depth>: depth of generated problem (for minizinc)" << endl
	     << "-i <fname>: input file name(.sas)" << endl
	     << "-o <fname>: output file name(.mzn minizinc, .lp gurobi)" << endl
	     << "-a <among?>: 1 -> true, 0 -> false (minizinc option)" << endl
	     << "-p <planning graph?>: 1 -> true, 0 -> false" << endl
	     << "-m <optimizatio level>: satisficing | step | global" << endl;
}

static void
show_usage_exit(string message) {
		cout << message << endl;
		usage();
		exit(-1);
}

static OptimizationLevel
select_opt_level(const string &optarg) {
	if (optarg == "global")
		return GlobalOptimal;
	else if (optarg == "step")
		return StepOptimal;
	else
		return Satisficing;
}

static ParallelLevel
select_para_level(const string &optarg) {
	if (optarg == "sequential")
		return Sequential;
	else
		return AllStep;
}

Config 
parse_config(int argc, char **argv){
	Config config;

	int res = 0;
	config.depth = 0;
	config.use_among = false;
	config.use_simple_pg = true;
	config.opt_level = Satisficing;
	config.para_level = AllStep;
	config.ifname = "";
	config.ofname = "";

	while ((res = getopt(argc, argv, "a:d:i:c:m:o:p:l:h")) != -1) {
		switch (res) {
			case 'd':
				config.depth = atoi(optarg);
				break;

			case 'a':
				config.use_among = (atoi(optarg) == 1 ? true : false);
				break;

			case 'c':
				config.incremental = (atoi(optarg) == 1 ? true : false);
				break;

			case 'p':
				config.use_simple_pg = (atoi(optarg) == 1 ? true : false);
				break;


			case 'i':
				config.ifname = string(optarg);
				break;

			case 'm':
				config.opt_level = select_opt_level(string(optarg));
				break;

			case 'l':
				config.para_level = select_para_level(string(optarg));
				break;

			case 'o':
				config.ofname = string(optarg);
				break;

			case 'h':
			default:
				usage();
				exit(-1);
		}
	}

	if (config.ifname == "") 
		show_usage_exit("input file name not specified: -i input_filename");

	if (config.ofname == "")
		show_usage_exit("output file name not specified: -o output_filename"
		"NOTE: If you want to use gurobi, this name should have "
		        "extension .lp");

	return config;
}
