#include <iostream>
#include <fstream>
#include <initializer_list>
#include <sstream>
#include <algorithm>
#include "clingo_generator.h"
using namespace std;

static string adder(initializer_list<string> list,string sep = ",") {
	string result = "";
	for(auto elem : list){
		result += elem + sep;
	}
	if(!result.empty())
		result.erase(prev(result.end()));
	return result;
}

static string f(Var i, Val j){
	stringstream ss;
	ss << "f(" << i << "," << j << ")";
	return ss.str();
}

static string  wrap(string outer, string inner){
	return outer + "(" + inner + ")";
}

template <typename T>
static string  holds(string prop, T step){
	stringstream ss;
	ss << prop << "," << step;
	return wrap("holds",ss.str());
}

static void rule(ofstream& ofs,string head,initializer_list<string> body){
	ofs << head;
	if(body.size())
	{
		ofs << " :- " << adder(body) << "." << endl;
	}
	else
	{
		ofs << "." << endl;
	}
}

void
clingo_generator_cond(const Problem *prob,const Config config, SimplePG &pg){
	cout << "clingo_ub called" << endl;
// 	unsigned int n_ops = prob->n_ops;
// 	unsigned int n_vars = prob->n_vars;
// 	auto operators = prob->operators;

	ofstream ofs(config.ofname, ofstream::out);

	ofs << "%% horizon n " << endl;
	ofs << "#const n = " << config.depth << "." << endl;
	ofs << "#show apply/2" << "." << endl;
	ofs << "#show ." << endl;
	rule(ofs,wrap("step","1..n"),{});
	rule(ofs,wrap("layer","0..n"),{});
	ofs << endl;

	ofs << "%% basic rules" << endl;
	rule(ofs,"1 {apply(A,I) : action(A)} 1",{"step(I)"});

	rule(ofs,"",{"apply(A,I)","demands(A,F)","not holds(F,I-1)","step(I)"});
	rule(ofs,"holds(F,I)",{"fired(E,I)","add(E,F)","step(I)","effect(E)"});
	rule(ofs,"changed(X,I)",{"fired(E,I)","add(E,f(X,Y))","step(I)","effect(E)"});
	rule(ofs,"holds(f(X,Y),I)",{"holds(f(X,Y),I-1)","step(I)","inertial(f(X,Y))","not changed(X,I)"});
	rule(ofs,"",{"goal(F)","not holds(F,n)"});
	ofs << endl;

	if (config.use_among) {
		rule(ofs,"",{"holds(f(X,Y),I)","holds(f(X,Z),I)","Y != Z","step(I)"});
	}

	for(Var i = 0; i < prob->vars.size();++i){
		if(!prob->vars[i].is_axiom()){
			for(Val val = 0; val < prob->vars[i].range; ++val){
				rule(ofs,wrap("inertial",f(i,val)),{});
			}
		}else{
			rule(ofs,holds(f(i,0),"I"),{"not "+holds(f(i,1),"I"),"layer(I)"});
			rule(ofs,"",{holds(f(i,0),"I"),holds(f(i,1),"I"),"layer(I)"});
			rule(ofs,"",{"not " + holds(f(i,0),"I"),"not " + holds(f(i,1),"I"),"layer(I)"});
		}
	}
	ofs << endl;

	ofs << "%% init" << endl;
	Var idx = 0;
	for(auto s: prob->init){
		if(!prob->vars[idx].is_axiom()){
			for(Val val = 0; val < prob->vars[idx].range; ++val){
				if (s == val) {
					rule(ofs,holds(f(idx,val),0),{});
				}
			}
		}
		++idx;
	}
	ofs << endl;

	ofs << "%% goal" << endl;
	for (auto p : prob->goal) {
		auto var = p.first;
		auto val = p.second;
		rule(ofs,wrap("goal",f(var,val)),{});
	}
	ofs << endl;

	ofs << "%% actions" << endl;
	int i = 0;
	unsigned int effect_index = 0;
	for(auto op : prob->operators){
		string name = "op" + to_string(i++);
		rule(ofs,wrap("action",name),{});
		for(auto prop : op.prevailConditions){
			rule(ofs,wrap("demands",adder({name,f(prop.first,prop.second)})),{});
		}

		for(auto effect : op.effects){
			if(effect.preval != NOT_REQUIRED)
				rule(ofs,wrap("demands",adder({name,f(effect.var,effect.preval)})),{});

			rule(ofs,wrap("add",adder({to_string(effect_index),f(effect.var,effect.postval)})),{});
			rule(ofs,wrap("effect",to_string(effect_index)),{});

			string body = "";
			for(auto prop: effect.assoc_condition){
				if(body.length())
					body += "," + wrap("holds",adder({f(prop.first,prop.second),"I-1"}));
				else
					body += wrap("holds",adder({f(prop.first,prop.second),"I-1"}));
			}

			if(body.length())
				rule(ofs,"fired("+to_string(effect_index)+",I)",{"layer(I)",wrap("apply",adder({name,"I"})), body});
			else
				rule(ofs,"fired("+to_string(effect_index)+",I)",{"layer(I)",wrap("apply",adder({name,"I"}))});

			effect_index++;
		}
	}

	ofs << "%% axioms" << endl;

	for(auto axiom : prob->axioms){
		string body = "";
		for(auto prop : axiom.conditions){
			if(body.empty())
				body = adder({holds(f(prop.first,prop.second),"I")});
			else
				body = adder({body,holds(f(prop.first,prop.second),"I")});
		}
		if(body.empty())
		{
			rule(ofs,holds(f(axiom.effect.first,axiom.effect.second),"I"),{"layer(I)"});
		}
		else
			rule(ofs,holds(f(axiom.effect.first,axiom.effect.second),"I"),{body,"layer(I)"});
	}

}

