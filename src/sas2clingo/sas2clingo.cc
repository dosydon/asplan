#include <unistd.h>
#include <iostream>
#include <cassert>
#include <limits.h>
#include <chrono>
#include "pgraph.h"
#include "model.h"
#include "config.h"
#include "measure.h"
#include "config.h"
#include "clingo_generator.h"
using namespace std;

void
clingo_main(Problem *prob,const Config config,SimplePG &pg) {
// 	assert(config.depth > 0);

	// auto min_cost = calc_min_cost(prob);
	cout << "cling main called" << endl;

	start_timer();
	if(is_prob_with_cond(prob))
		clingo_generator_cond(prob, config, pg);
	else
		clingo_generator(prob, config, pg);
	stop_timer("clingo_generate");
}

int
main(int argc, char **argv) {
	Config config = parse_config(argc,argv);


	/* XXX: (zinc) time-wasting, it is enough to parse once, gen_pg once */
	start_timer();
	Problem *prob = new Problem(config.ifname);
	stop_timer("parse");

	/* XXX: (zinc) time-wasting, it is enough to parse once, to gen_pg once */
	SimplePG pg;
	Depth pg_depth = 1;
	if (config.use_simple_pg) {
		start_timer();
		pg_depth = simple_extract_pg(prob, pg);
		stop_timer("pg_extract");

		cout << "lower_bound of depth obtained from pg: " << pg_depth << endl;
	}

	clingo_main(prob,config, pg);

	delete prob;
	config.dump();
}
