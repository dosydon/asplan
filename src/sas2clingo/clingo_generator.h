#pragma once

#include "model.h"
#include "pgraph.h"
#include "config.h"

void clingo_generator(const Problem *prob,const Config config, SimplePG &pg);
void clingo_generator_cond(const Problem *prob,const Config config, SimplePG &pg);
void clingo_incremental(const Problem *prob,const Config config, SimplePG &pg);
