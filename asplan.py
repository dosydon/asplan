#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import subprocess
import time
import functools
import psutil
import json
import os
import errno
import resource
import clingo_utils
from command import *
from executor import Executor
from log import *
import utils
import itertools
SAT_CODE = 10
UNSAT_CODE = 20
class ClingoException(Exception):
    pass

def clingo_main(args,log):
    ex = Executor(log,args.time_limit)
    args.depth = 0
    removed = False
    utils.clean()
    ex.run(TranslateCmd(args))
    if not os.path.isfile("output.sas"):
        raise ClingoException("output.sas not found")

    ex.run(PreprocessCmd(args))
    if args.axiom:
        ex.run(EncodeCmd(args))
        with open('output.sas','r') as f:
            if 'removed_operator' in f.read():
                removed = True
        if (not removed) and args.smart and hasattr(args, 'translate_args'):
            del args.translate_args
            ex.run(TranslateCmd(args))

    depth = 1
    while True:
        print("now at depth {}".format(depth))
        args.depth = depth
        ex.run(Sas2ClingoCmd(args))

        if args.separate:
            ex.run(GringoCmd(args))
            res = ex.run(ClaspCmd(args))
        else:
            res = ex.run(ClingoCmd(args))
        print("CLINGO")
        print("retcode:{}",res.retcode)

        if res.retcode in [SAT_CODE,30]:
            json_obj = json.loads(res.out)
            print(json_obj["Result"] == "SATISFIABLE")
            print("Solution Found!")
            print("solution_depth:{}".format(depth))

            with open("sas_plan",'w') as of:
                for action in clingo_utils.get_actions_from_json(json_obj):
                    print('(' + action + ')',file=of)

            break

        elif not res.retcode in [SAT_CODE,UNSAT_CODE,30]:
            raise ClingoException("retcode:{}".format(res.retcode))
        depth += 1

    if removed and os.path.isfile("sas_plan"):
        ex.run(DecodeCmd(args))
    if args.validate and os.path.isfile("sas_plan"):
        ex.run(ValidateCmd(args))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    utils.common_opts(parser)
    parser.add_argument("--among",help="enable among constraints",action="store_true")
    parser.add_argument("--separate",help="1:gringo+clasp 0:clingo",action="store_true")
    parser.add_argument("--opt_level",help="specify optimization level(satisficing|step|global)",default="satisficing")
    parser.add_argument("--para_level",help="specify para_level(sequential|all_step)",default="all_step")
    parser.add_argument("--depth",help="depth",type=int,default=0)
    subparsers = parser.add_subparsers(help='sub-command help')

    translate_parser = subparsers.add_parser('translate',help=' help')
    translate_parser.add_argument('translate_args',help='translate args',type=str,default=' ')

    args = parser.parse_args()
    print(args)
    
    utils.set_limits(args)
    log = []

    try:
        args.output = 'output.lp'
        clingo_main(args,log)
    except Exception as e:
        raise e
    finally:
        verbose_log(log)
        aggregate_log(log)
        print("MAXRSS:{:d}".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss + resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss))
