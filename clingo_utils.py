#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import json
import re

def get_actions_from_json(json_obj):
    op_numbers = get_op_numbers_from_json(json_obj)
    return op_numbers_to_names(op_numbers)

def get_op_numbers_from_json(json_obj):
    plan = json_obj["Call"][0]["Witnesses"][0]["Value"]
    res = []
    for item in plan:
        m = re.match('apply\((.*),(\d+)\)',item)
        action = (m.group(1))
        step = (m.group(2))
        res.append((action,int(step)))
    return [item[0] for item in sorted(res,key=lambda tup: tup[1])]

def get_actions_from_file(filename):
    names = []
    with open(filename, "r") as myfile:
        flag = False
        for i,line in enumerate(myfile.readlines()):
            if flag:
                names.append(line.rstrip('\n'))
            flag = False
            if line == "begin_operator\n":
                flag = True
    return names

def op_numbers_to_names(op_numbers):
    operators = get_actions_from_file('output.sas')
    print(operators)
    res = []
    for op_num in op_numbers:
        print(op_num)
        m = re.match('op(\d+)',op_num)
        num = int(m.group(1))
        res.append((operators[num]))
    return res

def result(json_obj):
    return json_obj["Result"]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("ifname")
    args = parser.parse_args()

